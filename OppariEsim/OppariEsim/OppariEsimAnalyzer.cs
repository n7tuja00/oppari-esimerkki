using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace OppariEsim
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class OppariEsimAnalyzer : DiagnosticAnalyzer
    {
        private static DiagnosticDescriptor Rule = Rules.CoolRule;
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
        {
            get
            {
                return ImmutableArray.Create(Rule);
            }
        }

        public override void Initialize(AnalysisContext context)
        {
            // Call the analyser if syntax kind is variable declaration
            context.RegisterSyntaxNodeAction(AnalyzeVariable, SyntaxKind.VariableDeclaration);
        }

        // Analyse the variable to see if its cool enough.
        private void AnalyzeVariable(SyntaxNodeAnalysisContext nodeContext)
        {
            // Get the variable declaration node as variable declaration syntax.
            VariableDeclarationSyntax variableDeclaration = nodeContext.Node as VariableDeclarationSyntax;
            
            foreach(VariableDeclaratorSyntax variable in variableDeclaration.Variables)
            {
                // If variable is not cool enough, report the violation.
                if (!variable.Identifier.ValueText.ToLower().Contains("mycool"))
                {
                    nodeContext.ReportDiagnostic(Diagnostic.Create(Rule, variable.GetLocation()));
                }
            }
        }
    }
}
