﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.CodeAnalysis;

namespace OppariEsim
{
    public static class Rules
    {
        /*
        // Template of a rule that could be used to quickly define new rules.
        public static readonly DiagnosticDescriptor RuleTemplate = new DiagnosticDescriptor(
            id: "Rule000",
            title: "Title that will be seen as a title of the rule.",
            messageFormat: "Message that will be shown when violation happens.",
            category: "Code style",
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "Desc that will be avaible in SonarQube"
            );
        */

        // Cool variable rule
        public static readonly DiagnosticDescriptor CoolRule = new DiagnosticDescriptor(
            id: "Rule001",
            title: "Title that will be seen as a title of the rule.",
            messageFormat: "Message that will be shown when violation happens.",
            category: "Code style",
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "Desc that will be avaible in SonarQube"
            );
    }
}
